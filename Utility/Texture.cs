﻿using System.Drawing;
using System.Drawing.Imaging;
using OpenTK.Graphics.OpenGL;
using PixelFormat = OpenTK.Graphics.OpenGL.PixelFormat;

namespace Utility
{
    public class Texture
    {
        public int GlHandle { get; protected set; }
        public string Name { get; protected set; }

        public void Bind()
        {
            GL.BindTexture(TextureTarget.Texture2D, GlHandle);
        }

        internal Texture(string name)
        {
            GlHandle = GL.GenTexture();
            GL.BindTexture(TextureTarget.Texture2D, GlHandle);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMagFilter, (float)TextureMagFilter.Linear);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, (float)TextureMinFilter.Linear);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.GenerateMipmap, 1);

            using (var bmp = (Bitmap)Image.FromFile(@"Textures\" + name + ".png"))
            {
                bmp.RotateFlip(RotateFlipType.RotateNoneFlipY);
                var bits = bmp.LockBits(new Rectangle(0, 0, bmp.Width, bmp.Height), ImageLockMode.ReadOnly,
                                        System.Drawing.Imaging.PixelFormat.Format32bppArgb);

                GL.TexImage2D(TextureTarget.Texture2D, 0, PixelInternalFormat.Rgba, bits.Width, bits.Height, 0,
                              PixelFormat.Bgra, PixelType.UnsignedByte, bits.Scan0);

                bmp.UnlockBits(bits);
            }
        }
    }
}