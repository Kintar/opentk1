﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using OpenTK.Graphics.OpenGL;

namespace Utility
{
    public static class TextureLoader
    {
        private static IList<Texture> textures = new List<Texture>(); 
 
        public static Texture LoadTexture(string name)
        {
            var tex = textures.SingleOrDefault(x => x.Name == name);

            if (tex == null)
            {
                tex = new Texture(name);
                textures.Add(tex);
            }

            return tex;
        }

        public static void FreeTexture(Texture tex)
        {
            GL.DeleteTexture(tex.GlHandle);
            textures.Remove(tex);
        }

        public static void FreeAllTextures()
        {
            foreach (var tex in textures)
            {
                GL.DeleteTexture(tex.GlHandle);
            }

            textures.Clear();
        }
    }
}
