﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace OpenTK1
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            var window = new GameWindow();
            window.MakeCurrent();
            window.Run();
        }
    }
}
