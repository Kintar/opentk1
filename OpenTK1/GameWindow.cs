﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;
using OpenTK.Input;
using Utility;

namespace OpenTK1
{
    public class GameWindow : OpenTK.GameWindow
    {
        private bool exit;
        private Texture tex;
        private MouseState lastMouseState;

        public GameWindow()
        {
            Title = "OpenTK1";
            Width = 800;
            Height = 600;

            var device = DisplayDevice.Default;
            var top = (device.Bounds.Height/2) - (Height/2);
            var left = (device.Bounds.Width/2) - (Width/2);
            
            Location = new Point(left, top);
            GL.ClearColor(Color.Black);
            lastMouseState = OpenTK.Input.Mouse.GetState();
        }

        protected override void OnResize(EventArgs e)
        {
            GL.Viewport(0, 0, ClientRectangle.Width, ClientRectangle.Height);

            var orthoProjection = Matrix4.CreateOrthographicOffCenter(0, ClientRectangle.Width,
                                                                      ClientRectangle.Height, 0, -1, 1);

            GL.MatrixMode(MatrixMode.Projection);
            GL.LoadMatrix(ref orthoProjection);
        }

        protected override void OnRenderFrame(FrameEventArgs e)
        {
            GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);

            GL.MatrixMode(MatrixMode.Modelview);
            GL.LoadIdentity();

            GL.Enable(EnableCap.Texture2D);
            //GL.Enable(EnableCap.CullFace);
            tex.Bind();
            
            GL.Begin(BeginMode.Quads);
            GL.Color3(Color.White);

            var qtrWidth = ClientRectangle.Width/4;
            var qtrHeight = ClientRectangle.Height/4;

            GL.TexCoord2(0, 1);
            GL.Vertex2(qtrWidth, qtrHeight);

            GL.TexCoord2(1, 1);
            GL.Vertex2(qtrWidth * 3, qtrHeight);

            GL.TexCoord2(1, 0);
            GL.Vertex2(qtrWidth * 3, qtrHeight * 3);

            GL.TexCoord2(0, 0);
            GL.Vertex2(qtrWidth, qtrHeight * 3);

            GL.End();

            SwapBuffers();
        }

        protected override void OnClosed(EventArgs e)
        {
            TextureLoader.FreeAllTextures();
        }

        protected override void OnKeyPress(KeyPressEventArgs e)
        {
            if (e.KeyChar == 27) exit = true;
        }

        protected override void OnUpdateFrame(FrameEventArgs e)
        {
            if (exit) Close();

            var currentMouseState = OpenTK.Input.Mouse.GetState();

            var buttonStateChanged = currentMouseState.LeftButton != lastMouseState.LeftButton;

            if (buttonStateChanged)
            {
                if (currentMouseState.IsButtonDown(MouseButton.Left))
                {
                    Console.WriteLine("Click at {0},{1}", Mouse.X, Mouse.Y);
                }
            }

            lastMouseState = currentMouseState;

            base.OnUpdateFrame(e);
        }

        protected override void OnLoad(EventArgs e)
        {
            tex = TextureLoader.LoadTexture("floor-tiles-20x20");
        }

    }
}
